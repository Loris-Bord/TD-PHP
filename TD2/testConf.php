<?php
// On inclut les fichiers de classe PHP pour pouvoir se servir de la classe Config.
// require_once évite que Config.php soit inclus plusieurs fois,
// et donc que la classe Config soit déclaré plus d'une fois.
use config\Conf;

require_once 'Conf.php';

// On affiche le login de la base de donnees
echo Conf::getLogin() . "\n" . Conf::getHostname() . "\n" . Conf::getDatabase() . "\n" . Conf::getPassword();
?>
