<?php

use model\Model;
use model\ModelVoiture;

require_once 'Model.php';
class Voiture {

    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque() : string {
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int $nbSieges
    ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation,0, 8);
        $this->nbSieges = $nbSieges;
    }

    // une methode d'affichage.
    public function afficher() {
        echo "Marque de la voiture : $this->marque,
              Couleur : $this->couleur,
              Immatriculation : $this->immatriculation,
              nbSieges : $this->nbSieges";
    }

    /**
     * @return mixed
     */
    public function getCouleur() : string
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     */
    public function setCouleur(string $couleur): void
    {
        $this->couleur = $couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed $immatriculation
     */
    public function setImmatriculation(string $immatriculation): void
    {
        $this->immatriculation = substr($immatriculation,0,8);
    }

    /**
     * @return mixed
     */
    public function getNbSieges() : int
    {
        return $this->nbSieges;
    }

    /**
     * @param mixed $nbSieges
     */
    public function setNbSieges(int $nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : ModelVoiture {
        return new ModelVoiture($voitureFormatTableau['marque'],$voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    public static function getVoiture() : array {
        $tabVoitures = [];
        $pdoStatement = Model::getPdo()->query('SELECT * FROM voiture');
        $tabVoitureFormatTableau = $pdoStatement->fetch();
        foreach ($pdoStatement as $tabVoitureFormatTableau){
            $tabVoitures[] = ModelVoiture::construire($tabVoitureFormatTableau);
        }
        return $tabVoitures;
    }


}
?>

