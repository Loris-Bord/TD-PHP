<?php

use model\Model;

class Trajet {

    private int $id;
    private string $depart;
    private string $arrivee;
    private string $date;
    private int $nbPlaces;
    private int $prix;
    private string $conducteurLogin;

    /**
     * @param int $id
     * @param string $depart
     * @param string $arrivee
     * @param string $date
     * @param int $nbPlaces
     * @param int $prix
     * @param string $conducteurLogin
     */
    public function __construct(int $id, string $depart, string $arrivee, string $date, int $nbPlaces, int $prix, string $conducteurLogin)
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
    }

    public function afficher(){
        echo "id : $this->id,
        depart : $this->depart,
        arrivee : $this->arrivee,
        date : $this->date,
        nbPlaces : $this->nbPlaces,
        prix : $this->prix,
        conducteurLogin : $this->conducteurLogin";
    }

    public static function construire(array $trajetFormatTableau) : Trajet {
        return new Trajet($trajetFormatTableau['id'],$trajetFormatTableau['depart'], $trajetFormatTableau['arrivee'], $trajetFormatTableau['date'], $trajetFormatTableau['nbPlaces'], $trajetFormatTableau['prix'], $trajetFormatTableau['conducteurLogin']);
    }

    public static function getTrajet() : array {
        $tabTrajets = [];
        $pdoStatement = Model::getPdo()->query('SELECT * FROM trajet');
        $tabVoitureFormatTableau = $pdoStatement->fetch(PDO::FETCH_BOTH);
        foreach ($pdoStatement as $tabVoitureFormatTableau){
            $tabTrajets[] = Trajet::construire($tabVoitureFormatTableau);
        }
        return $tabTrajets;
    }


}
?>