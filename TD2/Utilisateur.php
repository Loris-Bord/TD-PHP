<?php

use model\Model;

class Utilisateur {

    private $login;
    private $nom;
    private $prenom;

    /**
     * @param $login
     * @param $nom
     * @param $prenom
     */
    public function __construct($login, $nom, $prenom){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    /**
     * @return mixed
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return mixed
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * @return mixed
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    public function afficher(){
        echo "login : $this->login,
              nom : $this->nom,
              prenom : $this->prenom";
    }

    public static function construire(array $UtilisateurFormatTableau) : Utilisateur {
        return new Utilisateur($UtilisateurFormatTableau['login'],$UtilisateurFormatTableau['nom'], $UtilisateurFormatTableau['prenom']);
    }

    public static function getUtilisateurs() : array {
        $tabUtilisateurs = [];
        $pdoStatement = Model::getPdo()->query('SELECT * FROM Utilisateur');
        $tabVoitureFormatTableau = $pdoStatement->fetch(PDO::FETCH_BOTH);
        foreach ($pdoStatement as $tabVoitureFormatTableau){
            $tabUtilisateurs[] = Utilisateur::construire($tabVoitureFormatTableau);
        }
        return $tabUtilisateurs;
    }



}
?>