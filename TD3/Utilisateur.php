<?php

use model\Model;

require_once 'Model.php';
require_once 'Trajet.php';

class Utilisateur {

    private string $login;
    private string $nom;
    private string $prenom;

    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    public static function construire(array $utilisateurTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"]
        );
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    // une methode d'affichage.
    public function afficher() {
        echo "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
    }

    public function __toString() : string {
        return "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
    }

    /**
     * @return Utilisateur[]
     */
    public static function getUtilisateurs() : array {
        $pdoStatement = Model::getPdo()->query("SELECT * FROM utilisateur");

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = static::construire($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public static function getTrajets($login) : array{
        $sql = "SELECT * FROM trajet t JOIN passager p ON p.trajetId = t.id WHERE p.passagerLogin=:idTag";
        $tabTrajet = [];
        $pdoStatement = Model::getPdo()->prepare($sql);

        // Tableau d'arguments
        $values = array(
            "idTag" => $login
        );

        // On execute la requete avec les arguments
        $pdoStatement->execute($values);

        foreach ($pdoStatement as $UtilisateurFormatTableau){
            $tabTrajet[] = Trajet::construire($UtilisateurFormatTableau);
        }

        return $tabTrajet;
    }
}