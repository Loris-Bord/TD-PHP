<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div>
    <form method="get" action="formGetTrajets.php">
        <fieldset>
            <legend>Trouve les trajets passagers avec un login :</legend>
            <p>
                <label for="login_id">Login</label> :
                <input type="text" placeholder="dupuisl" name="Login" id="login_id" required/>
            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
<?php

require_once 'Model.php';
require_once 'ModelVoiture.php';
require_once 'Utilisateur.php';

function getTrajetsPassagerParLogin(string $login) : array {
    return Utilisateur::getTrajets($login);
}

if (isset($_GET['Login'])) {
    $v = getTrajetsPassagerParLogin($_GET['Login']);
    foreach ($v as $trajet){
        $trajet->afficher();
    }
}
?>
</body>
</html>
