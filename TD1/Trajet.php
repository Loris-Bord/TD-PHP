<?php
class Trajet {

    private $id;
    private $depart;
    private $arrivee;
    private $date;
    private $nbPlaces;
    private $prix;
    private $conducteurLogin;

    /**
     * @param $id
     * @param $depart
     * @param $arrivee
     * @param $date
     * @param $nbPlaces
     * @param $prix
     * @param $conducteurLogin
     */
    public function __construct($id, $depart, $arrivee, $date, $nbPlaces, $prix, $conducteurLogin){
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->nbPlaces = $nbPlaces;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
    }


}
?>