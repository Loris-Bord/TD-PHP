<?php
class Utilisateur {

    private $login;
    private $nom;
    private $prenom;

    /**
     * @param $login
     * @param $nom
     * @param $prenom
     */
    public function __construct($login, $nom, $prenom){
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

}
?>