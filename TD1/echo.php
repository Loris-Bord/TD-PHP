<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>

<body>
Voici le résultat du script PHP :
<?php
// Ceci est un commentaire PHP sur une ligne
/* Ceci est le 2ème type de commentaire PHP
sur plusieurs lignes */

// On met la chaine de caractères "hello" dans la variable 'texte'
// Les noms de variable commencent par $ en PHP
$texte = "hello world !";

// On écrit le contenu de la variable 'texte' dans la page Web
echo $texte;

  /*  $prenom = "Marc";
     echo "Bonjour\n" . $prenom;
     echo "Bonjour\n $prenom\n";
     echo 'Bonjour\n $prenom';

     echo $prenom;
     echo "$prenom";*/

     $marque = "renault";
     $couleur = "bleu";
     $immatriculation = "256-AB-34";
     $nbSieges = 5;

     echo "<p> ModelVoiture $immatriculation de marque $marque (couleur $couleur, $nbSieges sieges) </p>\n";

     $voiture = [
             "marque" => "peugeot",
             "couleur" => "blanc",
              "immatriculation" => "256-EB-45",
              "nbSieges" => 4
     ];

    $voiture2 = [
        "marque" => "renault",
        "couleur" => "beige",
        "immatriculation" => "257-EB-45",
        "nbSieges" => 5
    ];

     foreach ($voiture as $cle => $valeur){
         print_r("$cle : $valeur\n");
     }

    // print_r("\nVoiture $voiture[immatriculation] de marque $voiture[marque] (couleur $voiture[couleur], $voiture[nbSieges] sieges)");

     $voitures = [
             0 => $voiture,
             1 => $voiture2
     ];

     $voitureListeVide = [

     ];
 echo "<li><span>Liste de voitures :</span></li>";
     echo "<ul>";
     if(empty($voitures)) echo "la liste est vide";
    for($i = 0; $i < count($voitures); $i++){
        echo "<li>";
        foreach ($voitures[$i] as $cle => $valeur){
            echo "$cle : $valeur\n";
        }
        echo "</li>";
    }
     echo "</ul>";


   //  print_r($voitures[2]);
?>

<ul>
    <li><span>Liste de voitures :</span></li>
    <ul>
        <li><?php foreach ($voitures[0] as $cle => $valeur){
               echo "$cle : $valeur\n";
            }
            ?></li>
        <li><?php foreach ($voitures[1] as $cle => $valeur){
                echo "$cle : $valeur\n";
            }
            ?></li>
    </ul>
</ul>

</body>
</html>