<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Model\HTTP\Session;
use App\Covoiturage\Model\Repository\UtilisateurRepository;

class ConnexionUtilisateur
{
    // L'utilisateur connecté sera enregistré en session associé à la clé suivante
    private static string $cleConnexion = "_utilisateurConnecte";

    public static function connecter(string $loginUtilisateur): void
    {
        $session = Session::getInstance();
        $session->enregistrer(static::$cleConnexion, $loginUtilisateur);
    }

    public static function estConnecte(): bool
    {
        $session = Session::getInstance();
        return $session->contient(static::$cleConnexion);
    }

    public static function deconnecter(): void
    {
        $session = Session::getInstance();
        $session->supprimer(static::$cleConnexion);
    }

    public static function getLoginUtilisateurConnecte(): ?string
    {
        $session = Session::getInstance();
        if(!self::estConnecte()) return null;
        else return $session->lire(static::$cleConnexion);
    }

    public static function estUtilisateur($login): bool{
        return (strcmp($login, self::getLoginUtilisateurConnecte()) == 0) && self::estConnecte();
    }

    public static function estAdministrateur() : bool{
        return self::estConnecte() && ((new UtilisateurRepository())->select(self::getLoginUtilisateurConnecte())->isEstAdmin());
    }

    public static function estAdministrateurParLogin($login) : bool{
        return self::estConnecte() && ((new UtilisateurRepository())->select($login)->isEstAdmin());
    }




}