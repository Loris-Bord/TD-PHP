<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Config\Config;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\Repository\UtilisateurRepository;


class VerificationEmail
{
    public static function envoiEmailValidation(Utilisateur $utilisateur): void
    {
        $loginURL = rawurlencode($utilisateur->getLogin());
        $nonceURL = rawurlencode($utilisateur->getNonce());
        $absoluteURL = Config::getAbsoluteURL();
        $lienValidationEmail = "$absoluteURL?action=validerEmail&controller=utilisateur&login=$loginURL&nonce=$nonceURL";
        $corpsEmail = "<a href=\"$lienValidationEmail\">Validation</a>";

        // Temporairement avant d'envoyer un vrai mail
        mail($utilisateur->getEmailAValider(), $corpsEmail, $utilisateur->getNonce());
        MessageFlash::ajouter("success", $corpsEmail);
    }

    public static function traiterEmailValidation($login, $nonce): bool
    {
        $utilisateur = (new UtilisateurRepository())->select($login);
        if(isset($utilisateur)){
            $nonce = (strcmp($nonce, $utilisateur->getNonce()) == 0);
            if($nonce){
                $utilisateur->setEmail($utilisateur->getEmailAValider());
                $utilisateur->setEmailAValider("");
                $utilisateur->setNonce("");
                (new UtilisateurRepository())->update($utilisateur);
                return true;
            }

        }
        return false;
    }

    public static function aValideEmail(Utilisateur $utilisateur) : bool
    {
        // À compléter
        return true;
    }
}

