<?php

namespace App\Covoiturage\Controller;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Model\DataObject\Utilisateur;
use App\Covoiturage\Model\HTTP\Cookie;
use App\Covoiturage\Model\HTTP\Session;
use App\Covoiturage\Model\Repository\UtilisateurRepository;
use App\Covoiturage\Model\Repository\VoitureRepository;

class ControllerUtilisateur extends GenericController {
    public static function readAll(): void
    {
        $utilisateurs = (new UtilisateurRepository())->selectAll(); //appel au modèle pour gerer la BD
        self::afficheVue("view.php", ["utilisateurs" => $utilisateurs, "pagetitle" => "Liste des utilisateurs", "cheminVueBody" => "utilisateur/list.php"]);  //"redirige" vers la vue
    }

    public static function error(String $errorMessage = ""){
        self::afficheVue("view.php", ["pagetitle" => "erreur", "cheminVueBody" => "utilisateur/error.php"]);
    }

    public static function read(): void
    {
        $utilisateur = (new UtilisateurRepository())->select($_GET['login']);
        if (is_null($utilisateur)) self::error("echec lors de la lecture de l'utilisateur");
        else self::afficheVue("view.php", ["utilisateur" => $utilisateur, "pagetitle" => "Détails des utlisateurs", "cheminVueBody" => "utilisateur/detail.php"]);
    }

    public static function delete(): void
    {
        $login = $_GET['login'];
        if((new UtilisateurRepository())->select($login) == null){
            MessageFlash::ajouter("warning", "Le login n'existe pas !");
            self::readAll();
        }else if(!ConnexionUtilisateur::estUtilisateur($login)){
            MessageFlash::ajouter("danger", "l'utilisateur voulant etre supprimé ne correspond pas à l'utilisateur connecté !");
            self::readAll();
        }else {
            (new UtilisateurRepository())->delete($_GET['login']);
            $utilisateur = (new UtilisateurRepository())->selectAll();
            self::afficheVue("view.php", ["utilisateur" => $utilisateur, "pagetitle" => "Utilisateur supprimée", "cheminVueBody" => "utilisateur/deleted.php"]);
        }
    }

    public static function create(): void
    {
        self::afficheVue("view.php", ["pagetitle" => "Créer un utilisateur", "cheminVueBody" => "utilisateur/create.php"]);
    }

    public static function update() : void{
        $login = $_GET['login'];
        if((new UtilisateurRepository())->select($login) == null) {
            MessageFlash::ajouter("warning", "le login n'existe pas !");
            self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll");
        }else if(ConnexionUtilisateur::estUtilisateur($login) || ConnexionUtilisateur::estAdministrateur()) {
            $utilisateur = (new UtilisateurRepository())->select($login);
            if (is_null($login)) self::error("echec de l'update");
            else self::afficheVue("view.php", ["utilisateur" => $utilisateur, "cheminVueBody" => "utilisateur/update.php"]);
        }else{
            MessageFlash::ajouter("danger", "Vous n'etes pas cet utilisateur !");
            self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll");
        }
    }

    public static function updated() : void{
        //$voiture = new Voiture($_GET["marque"], $_GET["couleur"], $_GET["immatriculation"], $_GET["nbsiege"]);
        $utilisateur = (new UtilisateurRepository())->select($_GET['login']);
        if((new UtilisateurRepository())->select($_GET['login']) == null) {
            MessageFlash::ajouter("warning", "Le login n'existe pas !");
            self::readAll();
        }else {
            $mdpHache = $utilisateur->getMdpHache();
            if (strcmp($_GET['mdp2'], $_GET['mdp3']) != 0) {
                MessageFlash::ajouter("warning", "Mots de passe distincts.");
                //self::afficheVue("view.php", [ "login" => $_GET['login'], "cheminVueBody" => "utilisateur/update.php"]);
                $login = $_GET['login'];
                self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=update&login=$login");
            } else if (!MotDePasse::verifier($_GET['mdp'], $mdpHache)) {
                MessageFlash::ajouter("warning", "Mot de passe incorrect");
                $login = $_GET['login'];
                self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=update&login=$login");
            } else if(!ConnexionUtilisateur::estUtilisateur($_GET['login']) && !ConnexionUtilisateur::estAdministrateur()) {
                MessageFlash::ajouter("danger", "l'utilisateur mis à jour ne correspond pas à l'utilisateur connecté !");
                self::readAll();
            }else {
                $utilisateur->setMdpHache($_GET['mdp2']);
                $utilisateur->setNom($_GET['nom']);
                $utilisateur->setPrenom($_GET['prenom']);
                $boolAdmin = false;
                if(array_key_exists("estAdmin", $_GET)) {
                    $admin = $_GET['estAdmin'];
                    if (strcmp($admin, "on") == 0) $boolAdmin = true;
                }
                $utilisateur->setEstAdmin($boolAdmin);
                (new UtilisateurRepository())->update($utilisateur);
                $utilisateurs = (new UtilisateurRepository())->selectAll();
                MessageFlash::ajouter("success", "Mot de passe modifié avec succès !");
                self::afficheVue("view.php", ["utilisateurs" => $utilisateurs, "utilisateur" => $utilisateur, "login" => $utilisateur->getLogin(), "pagetitle" => "Utilisateur modifiée", "cheminVueBody" => "utilisateur/list.php"]);
            }
        }
    }

    public static function created(): void
    {
        if(!ConnexionUtilisateur::estAdministrateur()){
            $_GET['estAdmin'] = false;
        }
        if(!isset($_GET["login"]) || !isset($_GET["nom"]) || !isset($_GET["prenom"])){
            MessageFlash::ajouter("danger", "Login, nom ou prenom manquant.");
            self::create();
        }else if(strcmp($_GET['mdp'], $_GET['mdp2']) != 0){
            MessageFlash::ajouter("warning", "Mots de passe distincts.");
            self::create();
        }else if(!is_null((new UtilisateurRepository())->select($_GET['login']))){
            MessageFlash::ajouter("warning", "Login existante");
            self::create();
        }else {
            $utilisateur = Utilisateur::construireDepuisFormulaire($_GET);
            VerificationEmail::envoiEmailValidation($utilisateur);
            (new UtilisateurRepository())->save($utilisateur);
            $utilisateurs = (new UtilisateurRepository())->selectAll();
            MessageFlash::ajouter("sucess", "L'utilisateur a bien été créee !");
            self::afficheVue("view.php", ["utilisateurs" => $utilisateurs,"utilisateur" => $utilisateur, "pagetitle" => "Utilisateur créée", "cheminVueBody" => "utilisateur/list.php"]);
        }
    }

    public static function formulaireConnexion(): void{
        self::afficheVue("view.php", ["pagetitle" => "connexion au compte", "cheminVueBody" => "utilisateur/formulaireConnexion.php"]);
    }

    public static function connecter(): void{
        $utilisateur = (new UtilisateurRepository())->select($_GET['login']);
        $mdpHache = $utilisateur->getMdpHache();
        $login = $_GET['login'];
        if(!isset($login) || !isset($_GET['mdp'])){
            MessageFlash::ajouter("danger", "login ou mot de passe manquant !");
            self::formulaireConnexion();
        }else if(is_null($utilisateur) || !MotDePasse::verifier($_GET['mdp'], $mdpHache)){
            MessageFlash::ajouter("warning", "login ou mot de passe incorrect");
            self::formulaireConnexion();
        }else{
            MessageFlash::ajouter("success", "utilisateur connecté");
            ConnexionUtilisateur::connecter($login);
            self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=read&login=$login");
        }
    }

    public static function deconnecter(): void{
        ConnexionUtilisateur::deconnecter();
       // $utilisateurs = (new UtilisateurRepository())->selectAll();
        MessageFlash::ajouter("success", "vous avez été déconnecté !");
        self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=readAll");
    }

    public static function validerEmail(): void{
        if(isset($_GET['login']) && isset($_GET['nonce'])){
            $verif = VerificationEmail::traiterEmailValidation($_GET['login'], $_GET['nonce']);
            if($verif){
                $login = $_GET['login'];
                MessageFlash::ajouter("success", "Vérification effectuée avec succès !");
                self::redirection("https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=read&login=$login");
            }else{
                MessageFlash::ajouter("warning", "echec de la vérification");
                self::readAll();
            }
        }else{
            MessageFlash::ajouter("warning", "login ou nonce inexistant !");
            self::readAll();
        }
    }









  /*  public static function deposerCookie(){
        Cookie::enregistrer("TestCookie3", "OK", 3600);
    }

    public static function lireCookie(){
        print_r(Cookie::lire("TestCookie3"));
        Cookie::supprimer("TestCooki3");
    }*/

    public static function testSession() : void {
        $session = Session::getInstance();
        $session->enregistrer("utilisateur", "Loris Bord");
        echo $session->lire("utilisateur");
        $session->supprimer("utilisateur");
        echo $session->contient("utilisateur");
        $session->detruire();
    }

}