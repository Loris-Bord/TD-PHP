<?php

namespace App\Covoiturage\Controller;

use App\Covoiturage\Lib\PreferenceControleur;

abstract class GenericController {

    protected static function afficheVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ .  "/../view/$cheminVue"; // Charge la vue
    }

    public static function formulairePreference() : void {
        if(PreferenceControleur::existe()) $controllerPref = PreferenceControleur::lire();
        else $controllerPref = null;
        self::afficheVue("view.php", ["controllerPref" => $controllerPref,"pagetitle" => "préférence controller", "cheminVueBody" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void {
        if(isset($_GET['controleur_defaut'])) {
            $controleur = $_GET['controleur_defaut'];
            PreferenceControleur::enregistrer($controleur);
            self::afficheVue("view.php", ["pagetitle" => "préférence controller", "cheminVueBody" => "enregistrerPreference.php"]);
        }
    }

    public static function redirection($url): void{
        header("Location: $url");
        exit();
    }

}