<?php

//require_once __DIR__ . '/../Model/Voiture.php'; // chargement du modèle
namespace App\Covoiturage\Controller;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Model\Repository\AbstractRepository;
use \App\Covoiturage\Model\Repository\VoitureRepository;
use \App\Covoiturage\Model\DataObject\Voiture;
class ControllerVoiture extends GenericController
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll(): void
    {
        $voitures = (new VoitureRepository())->selectAll(); //appel au modèle pour gerer la BD
        self::afficheVue("view.php", ["voitures" => $voitures, "pagetitle" => "Liste des voitures", "cheminVueBody" => "voiture/list.php"]);  //"redirige" vers la vue
    }

    public static function read(): void
    {
        $voiture = (new VoitureRepository())->select($_GET['immat']);
        if (is_null($voiture)) self::error("echec lors de la lecture de la voiture");
        else self::afficheVue("view.php", ["voiture" => $voiture, "pagetitle" => "Détails des voitures", "cheminVueBody" => "voiture/detail.php"]);
    }

    public static function create(): void
    {
        self::afficheVue("view.php", ["pagetitle" => "Créer une voiture", "cheminVueBody" => "voiture/create.php"]);
    }

    public static function error(String $errorMessage=""){
        self::afficheVue("view.php", ["errorMessage" => $errorMessage,"pagetitle" => "erreur", "cheminVueBody" => "voiture/error.php"]);
    }

    public static function created(): void
    {
        //$voiture = new VoitureRepository($_GET["marque"], $_GET["couleur"], $_GET["immatriculation"], $_GET["nbsiege"]);
        if(!isset($_GET["marque"]) || !isset($_GET["couleur"]) || !isset($_GET["immatriculation"]) || !isset($_GET["nbSieges"])){
            MessageFlash::ajouter("danger", "Immatriculation, marque, couleur ou nombre de sièges manquant.");
            self::create();
        }else if(!is_null((new VoitureRepository())->select($_GET['immatriculation']))){
            MessageFlash::ajouter("warning", "Immatriculation existante");
            self::create();
        }else {
            MessageFlash::ajouter("success", "La voiture a bien été créee !");
            $voiture = (new VoitureRepository())->construire($_GET);
            (new VoitureRepository())->save($voiture);
            $voitures = (new VoitureRepository())->selectAll();
            self::afficheVue("view.php", ["voitures" => $voitures, "voiture" => $voiture, "pagetitle" => "Voiture créée", "cheminVueBody" => "voiture/list.php"]);
        }
    }

    public static function delete(): void
    {
        (new VoitureRepository())->delete($_GET['immat']);
        $voitures = (new VoitureRepository())->selectAll();
        self::afficheVue("view.php", ["voitures" => $voitures,"pagetitle" => "Voiture supprimée", "cheminVueBody" => "voiture/deleted.php"]);
    }

    public static function update() : void{
        $immatriculation = $_GET['immat'];
        $voiture = (new VoitureRepository())->select($immatriculation);
        if(is_null($immatriculation)) self::error("echec de l'update");
        else self::afficheVue("view.php", ["voiture" => $voiture, "cheminVueBody" => "voiture/update.php"]);
    }

    public static function updated() : void{
        //$voiture = new Voiture($_GET["marque"], $_GET["couleur"], $_GET["immatriculation"], $_GET["nbsiege"]);
        $voiture = (new VoitureRepository())->construire($_GET);
        (new VoitureRepository())->update($voiture);
        $voitures = (new VoitureRepository())->selectAll();
        self::afficheVue("view.php", ["voitures" => $voitures,"voiture" => $voiture, "immatriculation" => $voiture->getImmatriculation(), "pagetitle" => "Voiture modifiée", "cheminVueBody" => "voiture/updated.php"]);
    }


}

?>