<?php

namespace App\Covoiturage\Model\DataObject;

use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Model\Repository\UtilisateurRepository;

class Utilisateur extends AbstractDataObject {

    private string $login;
    private string $nom;
    private string $prenom;
    private string $mdpHache;
    private bool $estAdmin;
    private string $email;
    private string $emailAValider;
    private string $nonce;

    public function __construct(string $login, string $nom, string $prenom, string $mdpHache, bool $estAdmin, string $email, string $emailAValider, string $nonce)
    {
        $this->login = $login;
        $this->nom = $nom;
        $this->prenom = $prenom;
        $this->mdpHache = $mdpHache;
        $this->estAdmin = $estAdmin;
        $this->email = $email;
        $this->emailAValider = $emailAValider;
        $this->nonce = $nonce;
    }

    public function getLogin(): string
    {
        return $this->login;
    }

    public function setLogin(string $login): void
    {
        $this->login = $login;
    }

    public function getNom(): string
    {
        return $this->nom;
    }

    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    public function getPrenom(): string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    /**
     * @return string
     */
    public function getMdpHache(): string
    {
        return $this->mdpHache;
    }

    /**
     * @param string $mdpHache
     */
    public function setMdpHache(string $mdpHache): void
    {
        $this->mdpHache = MotDePasse::hacher($mdpHache);
    }

    /**
     * @return bool
     */
    public function isEstAdmin(): bool
    {
        return $this->estAdmin;
    }

    /**
     * @param bool $estAdmin
     */
    public function setEstAdmin(bool $estAdmin): void
    {
        $this->estAdmin = $estAdmin;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmailAValider(): string
    {
        return $this->emailAValider;
    }

    /**
     * @param string $emailAValider
     */
    public function setEmailAValider(string $emailAValider): void
    {
        $this->emailAValider = $emailAValider;
    }

    /**
     * @return string
     */
    public function getNonce(): string
    {
        return $this->nonce;
    }

    /**
     * @param string $nonce
     */
    public function setNonce(string $nonce): void
    {
        $this->nonce = $nonce;
    }


    // une methode d'affichage.
    public function afficher() {
        echo "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
    }

    public function __toString() : string {
        return "<p> Utilisateur {$this->prenom} {$this->nom} de login {$this->login} </p>";
    }

    public function convertBoolToInt(bool $chaine): int{
        return $chaine ? 1 : 0;
    }


    public function formatTableau(): array
    {
        $estAdmin = self::convertBoolToInt($this->estAdmin);
        return array(
            "clePrimaireTag" => $this->login,
            "nomTag" => $this->nom,
            "prenomTag" => $this->prenom,
            "mdpHacheTag" => $this->mdpHache,
            "estAdminTag" => $estAdmin,
            "emailTag" => $this->email,
            "emailAValiderTag" => $this->emailAValider,
            "nonceTag" => $this->nonce,
        );
    }

    public static function construireDepuisFormulaire (array $tableauFormulaire) : Utilisateur{
        $mdpHache = MotDePasse::hacher($tableauFormulaire['mdp']);
        $boolAdmin = false;
        $nonce = MotDePasse::genererChaineAleatoire();
        if(array_key_exists("estAdmin", $tableauFormulaire)) {
            $admin = $tableauFormulaire['estAdmin'];
            if (strcmp($admin, "on") == 0) $boolAdmin = true;
        }
        return new Utilisateur($tableauFormulaire['login'],$tableauFormulaire['nom'], $tableauFormulaire['prenom'], $mdpHache, $boolAdmin, "", $tableauFormulaire['email'], $nonce);
    }



}