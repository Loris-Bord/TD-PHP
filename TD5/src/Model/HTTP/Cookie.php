<?php

namespace App\Covoiturage\Model\HTTP;
class Cookie {

    public static function enregistrer(string $cle, mixed $valeur, ?int $dureeExpiration = null): void {
        $valeurSerialize = serialize($valeur);
        if(is_null($dureeExpiration)){
            setcookie($cle, $valeurSerialize, 0);
        }else{
            setcookie($cle, $valeurSerialize, time() + $dureeExpiration);
        }
    }

    public static function lire(string $cle): mixed{
        return unserialize($_COOKIE[$cle]);
    }

    public static function contient($cle) : bool {
        return isset($_COOKIE[$cle]);
    }

    public static function supprimer($cle) : void {
        unset($_COOKIE[$cle]);
        setcookie("$cle", "", 1);
    }



}