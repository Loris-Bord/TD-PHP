<?php
namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\Repository\DatabaseConnection;
use App\Covoiturage\Model\DataObject\Voiture;

class VoitureRepository extends AbstractRepository {

    public function construire(array $voitureFormatTableau): Voiture
    {
        return new Voiture($voitureFormatTableau['marque'], $voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    protected function getNomTable(): string
    {
        return "voiture";
    }

    protected function getNomClePrimaire(): string
    {
       return "immatriculation";
    }

    protected function getNomsColonnes(): array
    {
        return array(
            "marque" => "marque",
            "couleur" => "couleur",
            "nbSieges" => "nbSieges"
        );
    }
}