<?php

namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\AbstractDataObject;

abstract class AbstractRepository {

    public function selectAll(): array
    {
        $tabObjets = [];
        $nom = $this->getNomTable();
        $pdoStatement = DatabaseConnection::getPdo()->query("SELECT * FROM $nom");
        //$tabObjetFormatTableau = $pdoStatement->fetch();
        foreach ($pdoStatement as $tabObjetFormatTableau) {
            $tabObjets[] = $this->construire($tabObjetFormatTableau);
        }
        return $tabObjets;
    }

    protected abstract function getNomTable(): string;

    protected abstract function construire(array $objetFormatTableau) : AbstractDataObject;

    public function select(string $valeurClePrimaire): ?AbstractDataObject
    {
        $nom = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();
        $sql = "SELECT * from $nom WHERE $clePrimaire=:clePrimaireTag";
        // Préparation de la requête
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);

        $values = array(
            "clePrimaireTag" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $objet = $pdoStatement->fetch();
        if (!$objet) return null;

        return $this->construire($objet);
    }

    protected abstract function getNomClePrimaire(): string;

    public function delete(string $valeurClePrimaire)
    {
        $nom = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();

        $sql = "DELETE FROM $nom WHERE $clePrimaire=:clePrimaireTag";
        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = array(
            "clePrimaireTag" => $valeurClePrimaire,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    public function update(AbstractDataObject $object): void{

        $table = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();
        $nomsColonnes = $this->getNomsColonnes();


        $sql = "UPDATE $table 
                SET $clePrimaire=:clePrimaireTag";

        foreach($nomsColonnes as $colonne){
           $sql .= ",$colonne=:$colonne" .  "Tag";
        }

        $sql .= " WHERE $clePrimaire=:clePrimaireTag";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = $object->formatTableau();
        $pdoStatement->execute($values);
    }

    protected abstract function getNomsColonnes(): array;

    public function save(AbstractDataObject $object): void{

        $table = $this->getNomTable();
        $clePrimaire = $this->getNomClePrimaire();
        $nomsColonnes = $this->getNomsColonnes();

        $sql = "INSERT INTO $table VALUES (:clePrimaireTag";

        foreach ($nomsColonnes as $colonne){
            $sql .= ",:$colonne" . "Tag";
        }
        $sql .= ")";

        $pdoStatement = DatabaseConnection::getPdo()->prepare($sql);
        $values = $object->formatTableau();
        $pdoStatement->execute($values);


    }



}