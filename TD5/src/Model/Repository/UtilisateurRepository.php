<?php

namespace App\Covoiturage\Model\Repository;

use App\Covoiturage\Model\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository {

    /**
     * @return Utilisateur[]
     */
   /* public static function getUtilisateurs() : array {
        $pdoStatement = DatabaseConnection::getPdo()->query("SELECT * FROM Utilisateur");

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = static::construire($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }*/
    public function convertIntToBool(int $chaine): bool{
        return $chaine == 1;
    }

    public function construire(array $utilisateurTableau) : Utilisateur {
        $estAdminBool = self::convertIntToBool($utilisateurTableau['estAdmin']);
        return new Utilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"],
            $utilisateurTableau["mdpHache"],
            $estAdminBool,
            $utilisateurTableau["email"],
            $utilisateurTableau["emailAValider"],
            $utilisateurTableau["nonce"],
        );
    }


    protected function getNomTable(): string
    {
        return "Utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return array(
            "nom" => "nom",
            "prenom" => "prenom",
            "mdpHache" => "mdpHache",
            "estAdmin" => "estAdmin",
            "email" => "email",
            "emailAValider" => "emailAValider",
            "nonce" => "nonce",
        );
    }



    protected function swapMailAValider(): void{
        $sql = "UPDATE Utilisateur SET email=:";
    }
}