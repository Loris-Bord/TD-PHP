<?php
    $voiturePref = "";
    $userPref = "";
    $trajetPref = "";
    if(!is_null($controllerPref)){
        if(strcmp($controllerPref, "trajet") == 0) $trajetPref = "checked";
        elseif (strcmp($controllerPref, "voiture") == 0) $voiturePref = "checked";
        else $userPref = "checked";
    }

?>

<form method="get" action="frontController.php">
    <fieldset>
        <legend>formulaire de préférence controller :</legend>
    <p>
        <input type="radio" id="voitureId" name="controleur_defaut" value="voiture" <?= $voiturePref;?>>
        <label for="voitureId">Voiture</label>

        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur" <?= $userPref;?>>
        <label for="utilisateurId">Utilisateur</label>

         <input type="radio" id="trajetId" name="controleur_defaut" value="trajet" <?= $trajetPref;?>>
        <label for="trajetId">Trajet</label>
    </p>
        <p>
            <input type="hidden" name="action" value="enregistrerPreference">
            <input type="submit" value="Envoyer" />
        </p>
        <fieldset>
</form>
