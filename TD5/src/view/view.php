<?php
    use \App\Covoiturage\Lib\MessageFlash;
    use \App\Covoiturage\Lib\ConnexionUtilisateur;
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title><?php echo $pagetitle; ?></title>
    <link href="css/style.css" rel="stylesheet" type="text/css" media="screen">
</head>
<body>
<header>
    <nav>
        <ul>
             <li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll">Page d'accueil</a></li>
            <li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll&controller=voiture">Voitures</a></li>
             <li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll&controller=utilisateur">Utilisateurs</a></li>
             <li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=readAll&controller=trajet">Trajets</a></li>
             <li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=formulairePreference">
                    <img src="/home/ann2/bordl/public_html/td-php/TD5/web/img/heart.png" alt="coeur"/></a></li>
            <?php
                if(!ConnexionUtilisateur::estConnecte()){
                    echo '<li><a href="https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?action=formulaireConnexion">
                    <img src="/home/ann2/bordl/public_html/td-php/TD5/web/img/heart.png" alt="connexion"/></a></li>';
                }else{
                    $login = ConnexionUtilisateur::getLoginUtilisateurConnecte();
                    echo "<li><a href=\"https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=read&login=$login\">
                    <img src=\"/home/ann2/bordl/public_html/td-php/TD5/web/img/heart.png\" alt=\"détails\"/></a></li>";

                    echo "<li><a href=\"https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=deconnecter\">
                    <img src=\"/home/ann2/bordl/public_html/td-php/TD5/web/img/heart.png\" alt=\"déconnexion\"/></a></li>";
                }
            ?>
        </ul>
    </nav>
    <?php
        if(MessageFlash::contientMessage("warning")){
            $tab = MessageFlash::lireMessages("warning");
            echo "<div class=\"alert alert-warning\">$tab[0]</div>";

        }else if(MessageFlash::contientMessage("success")){
            $tab = MessageFlash::lireMessages("success");
            echo "<div class=\"alert alert-success\">$tab[0]</div>";

        }else if(MessageFlash::contientMessage("info")){
            $tab = MessageFlash::lireMessages("info");
            echo "<div class=\"alert alert-info\">$tab[0]</div>";

        }else if(MessageFlash::contientMessage("danger")){
            $tab = MessageFlash::lireMessages("danger");
            echo "<div class=\"alert alert-danger\">$tab[0]</div>";
        }
    ?>
</header>
<main>
    <?php
        require __DIR__ . "/{$cheminVueBody}";
    ?>
</main>
<footer>
    <p>Site de covoiturage de Loris Bord</p>
</footer>
</body>
</html>

