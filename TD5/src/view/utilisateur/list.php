<?php
use App\Covoiturage\Lib\ConnexionUtilisateur;
foreach ($utilisateurs as $utilisateur) {
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    $loginURL = rawurlencode($utilisateur->getLogin());
    echo '<p> Utilisateur de login ' . "<a href=\"https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=read&login=$loginURL\"> $loginHTML </a>";
    if(ConnexionUtilisateur::estAdministrateur()) {
        echo " - " ." <a href=\"https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=update&login=$loginURL\">modifier</a>";
    }
}
echo '<p>' . "<a href=\"https://webinfo.iutmontp.univ-montp2.fr/~bordl/td-php/TD5/web/frontController.php?controller=utilisateur&action=create\"> créer </a>" . '</p>';
