<?php
use App\Covoiturage\Model\Repository\UtilisateurRepository;
use App\Covoiturage\Lib\ConnexionUtilisateur;
//$voiture = VoitureRepository::getVoitureParImmat($immatriculation);
$loginHTML = htmlspecialchars($utilisateur->getLogin());
$nomHTML = htmlspecialchars($utilisateur->getNom());
$prenomHTML = htmlspecialchars($utilisateur->getPrenom());
$admin = "";
if(ConnexionUtilisateur::estAdministrateurParLogin($utilisateur->getLogin())){
    $admin = "checked";
}
?>


<form method="get" action="frontController.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="login_id">Login</label> :
            <input type="text" placeholder="bordl" name="login" id="login_id" value=<?= $loginHTML;?> required/>
            <br>
            <label for="mdp_id">Mot de passe actuel</label> :
            <input type="text" placeholder="mdp" name="mdp" id="mdp_id" required/>
            <br>
            <label for="mdp2_id">Nouveau mot de passe</label> :
            <input type="password" placeholder="" name="mdp2" id="mdp2_id" required/>
            <br>
            <label for="mdp3_id">Vérification nouveau mot de passe</label> :
            <input type="password" placeholder="" name="mdp3" id="mdp3_id" required/>
            <br>
            <label for="nom_id">Nom</label> :
            <input type="text" placeholder="bord" name="nom" id="nom_id" value=<?= $prenomHTML;?> required/>
            <br>
            <label for="prenom_id">Prenom</label> :
            <input type="text" placeholder="loris" name="prenom" id="prenom_id" value=<?= $nomHTML;?> required/>
        </p>
        <?php
        $admin2 = \App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur();
        if($admin2){
            echo "<p class='InputAddOn'>";
            echo "<label class=\"InputAddOn-item\" for=\"estAdmin_id\">Administrateur</label>";
            echo "<input class=\"InputAddOn-field\" type=\"checkbox\" placeholder=\"\" name=\"estAdmin\" id=\"estAdmin_id\" $admin>";
            echo "</p>";

        }
        ?>
        <p>
            <input type="hidden" name="action" value="updated">
            <input type="hidden" name="controller" value="utilisateur">
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
