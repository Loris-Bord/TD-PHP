<?php
    use App\Covoiturage\Model\Repository\VoitureRepository;
    //$voiture = VoitureRepository::getVoitureParImmat($immatriculation);
    $immatriculationHTML = htmlspecialchars($voiture->getImmatriculation());
    $couleurHTML = htmlspecialchars($voiture->getCouleur());
    $marqueHTML = htmlspecialchars($voiture->getMarque());
    $nbSiegesHTML = htmlspecialchars($voiture->getnbSieges());
?>


<form method="get" action="frontController.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <p>
            <label for="immat_id">Immatriculation</label> :
            <input type="text" placeholder="256AB34" name="immatriculation" id="immat_id" value=<?= $immatriculationHTML;?> required/>
            <br>
            <label for="marque_id">Marque</label> :
            <input type="text" placeholder="Peugeot" name="marque" id="marque_id" value=<?= $marqueHTML;?> required/>
            <br>
            <label for="couleur_id">Couleur</label> :
            <input type="text" placeholder="Bleu" name="couleur" id="couleur_id" value=<?= $couleurHTML;?> required/>
            <br>
            <label for="nbsiege_id">nbSiege</label> :
            <input type="number" placeholder="2" name="nbSieges" id="nbsiege_id" value=<?= $nbSiegesHTML;?> required/>
        </p>
        <p>
            <input type="hidden" name="action" value="updated">
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>

