<?php

//require_once __DIR__ . '/../src/Controller/ControllerVoiture.php';
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';
use App\Covoiturage\Controller\ControllerVoiture;
use \App\Covoiturage\Lib\PreferenceControleur;

// instantiate the loader
$loader = new App\Covoiturage\Lib\Psr4AutoloaderClass();
// register the base directories for the namespace prefix
$loader->addNamespace('App\Covoiturage', __DIR__ . '/../src');
// register the autoloader
$loader->register();

// On récupère le controlleur passée dans l'URL
/*if(isset($_GET['controller'])){
    $controller = $_GET['controller'];
}else{
    $controller = "voiture";
}*/

// On recupère l'action passée dans l'URL
/*if(isset($_GET['action'])) {
    $action = $_GET['action'];
}else{
    $action = "readAll";
}*/
$controllerPreference = "voiture";

if(PreferenceControleur::existe()){
    $controllerPreference = ucfirst(PreferenceControleur::lire());
}

$controller = $_REQUEST['controller'] ?? $controllerPreference;

$className = ucfirst($controller);
$controllerClassName = "App\Covoiturage\Controller\Controller$className";

$action = $_REQUEST['action'] ?? "readAll";

// Appel de la méthode statique $action de ControllerVoiture
if(class_exists($controllerClassName)) {
    if (in_array($action, get_class_methods($controllerClassName))) {
        $controllerClassName::$action();
    } else {
        $controllerClassName::error();
    }
}else{
    ControllerVoiture::error();
}
?>