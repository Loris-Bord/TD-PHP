<?php

use model\ModelVoiture;

require_once ('../model/ModelVoiture.php'); // chargement du modèle
class ControllerVoiture {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function readAll() : void {
        $voitures = ModelVoiture::getVoitures(); //appel au modèle pour gerer la BD
        self::afficheVue("voiture/list.php", ["voitures" => $voitures]);  //"redirige" vers la vue
    }

    public static function read() : void{
        $voiture = ModelVoiture::getVoitureParImmat($_GET['immat']);
        if($voiture == null) self::afficheVue("voiture/error.php", ["voiture" => $voiture]);
        else self::afficheVue("voiture/detail.php", ["voiture" => $voiture]);
    }

    private static function afficheVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../view/$cheminVue"; // Charge la vue
    }

    public static function create() : void{
        self::afficheVue("voiture/create.php");
    }

    public static function created() : void{
        $voiture = new ModelVoiture($_GET["marque"],$_GET["couleur"],$_GET["immatriculation"],$_GET["nbsiege"]);
        $voiture->sauvegarder();
        self::readAll();
    }

    public static function delete() : void{
        ModelVoiture::deleteVoitureParImmat($_GET['immat']);
        self::readAll();
    }


}
?>