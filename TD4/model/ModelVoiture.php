<?php

use model\Model;

require_once 'Model.php';
class ModelVoiture {

    private string $marque;
    private string $couleur;
    private string $immatriculation;
    private int $nbSieges; // Nombre de places assises

    // un getter
    public function getMarque() : string {
        return $this->marque;
    }

    // un setter
    public function setMarque(string $marque) {
        $this->marque = $marque;
    }

    // un constructeur
    public function __construct(
        string $marque,
        string $couleur,
        string $immatriculation,
        int $nbSieges
    ) {
        $this->marque = $marque;
        $this->couleur = $couleur;
        $this->immatriculation = substr($immatriculation,0, 8);
        $this->nbSieges = $nbSieges;
    }

    // une methode d'affichage.
   /* public function afficher() {
        echo "Marque de la voiture : $this->marque,
              Couleur : $this->couleur,
              Immatriculation : $this->immatriculation,
              nbSieges : $this->nbSieges";
    }*/

    /**
     * @return mixed
     */
    public function getCouleur() : string
    {
        return $this->couleur;
    }

    /**
     * @param mixed $couleur
     */
    public function setCouleur(string $couleur): void
    {
        $this->couleur = $couleur;
    }

    /**
     * @return mixed
     */
    public function getImmatriculation() : string
    {
        return $this->immatriculation;
    }

    /**
     * @param mixed $immatriculation
     */
    public function setImmatriculation(string $immatriculation): void
    {
        $this->immatriculation = substr($immatriculation,0,8);
    }

    /**
     * @return mixed
     */
    public function getNbSieges() : int
    {
        return $this->nbSieges;
    }

    /**
     * @param mixed $nbSieges
     */
    public function setNbSieges(int $nbSieges): void
    {
        $this->nbSieges = $nbSieges;
    }

    public static function construire(array $voitureFormatTableau) : model\ModelVoiture {
        return new model\ModelVoiture($voitureFormatTableau['marque'],$voitureFormatTableau['couleur'], $voitureFormatTableau['immatriculation'], $voitureFormatTableau['nbSieges']);
    }

    public static function getVoitures() : array {
        $tabVoitures = [];
        $pdoStatement = Model::getPdo()->query('SELECT * FROM voiture');
        $tabVoitureFormatTableau = $pdoStatement->fetch();
        foreach ($pdoStatement as $tabVoitureFormatTableau){
            $tabVoitures[] = model\ModelVoiture::construire($tabVoitureFormatTableau);
        }
        return $tabVoitures;
    }

    public static function getVoitureParImmat(string $immatriculation) : ?model\ModelVoiture {
        $sql = "SELECT * from voiture WHERE immatriculation=:immatriculationTag";
        // Préparation de la requête
        $pdoStatement = Model::getPdo()->prepare($sql);

        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);

        // On récupère les résultats comme précédemment
        // Note: fetch() renvoie false si pas de voiture correspondante
        $voiture = $pdoStatement->fetch();
        if(!$voiture) return null;

        return static::construire($voiture);
    }

    public static function deleteVoitureParImmat(string $immatriculation) : void{
        $sql = "DELETE FROM voiture WHERE immatriculation=:immatriculationTag";
        $pdoStatement = Model::getPdo()->prepare($sql);
        $values = array(
            "immatriculationTag" => $immatriculation,
            //nomdutag => valeur, ...
        );
        // On donne les valeurs et on exécute la requête
        $pdoStatement->execute($values);
    }

    public function sauvegarder() : void{
        $sql = "INSERT INTO voiture (immatriculation,marque,couleur,nbSieges) VALUES (:immatTag, :marqueTag, :couleurTag, :nbSiegeTag)";
        // Préparation de la requete
        $pdoStatement = Model::getPdo()->prepare($sql);

        // Tableau d'arguments
        $values = array(
            "immatTag" => $this->immatriculation,
            "marqueTag" => $this->marque,
            "couleurTag" => $this->couleur,
            "nbSiegeTag" => $this->nbSieges
        );

        // On execute la requete avec les arguments
        $pdoStatement->execute($values);
    }



}
?>

